<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Crime extends Model {

	protected $fillable = ['latitude', 'longitude'];
	protected $hidden = ['created_at', 'updated_at'];

}
