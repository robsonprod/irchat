<?php namespace App\Http\Controllers;

use Input;
use App\Crime;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Illuminate\Http\Request;

class CrimeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$crimes = Crime::all();

		return response()->json($crimes, 200);
	}

	public function store()
	{
		$inputs = Input::all();

		$crime = Crime::create($inputs);

		if (!empty($crime)) return response()->json($inputs, 200);
		return response()->json('error ao salvar', 400);
	}

	public function buscaPorTipo($tipo) {
		if (!$tipo) return response()->json([], 200);

		$crimes = Crime::where('tipo_crime', '=', $tipo)->get();

		return response()->json($crimes->toArray() ,200); 
	}

}
