<?php


// Route::get('/', 'WelcomeController@index');

// Route::get('home', 'HomeController@index');

// Route::controllers([
// 	'auth' => 'Auth\AuthController',
// 	'password' => 'Auth\PasswordController',
// ]);

Route::group(['middleware' => 'cors', 'prefix' => 'api/v1'], function(){
	Route::get('crimes', 'CrimeController@index');
	Route::get('busca/{tipo}', 'CrimeController@buscaPorTipo');
	Route::post('create', 'CrimeController@store');

});